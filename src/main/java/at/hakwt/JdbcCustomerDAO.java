package at.hakwt;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class JdbcCustomerDAO implements CustomerDao {


    public void save(List<Customer> customers) {
        Connection connection;
        try {
            connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/customer-import", "root", "");
            for(Customer c : customers) {
                save(c, connection);
            }
        } catch (SQLException e) {
            System.err.println("Could not connect to database: " + e.getMessage());
            return;
        }

        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Could not close database connection");
        }

    }

    private void save(Customer customer, Connection connection) {
        try {
            PreparedStatement insert = connection.prepareStatement("insert into customer(imported_id, first_name, last_name, zip_code, city, street, email_address) values (?, ?, ?, ?, ?, ?, ?)");
            insert.setInt(1, customer.getCustomerNo());
            insert.setString(2, customer.getFirstName());
            insert.setString(3, customer.getLastName());
            insert.setString(4, customer.getZipCode());
            insert.setString(5, customer.getCity());
            insert.setString(6, customer.getStreet());
            insert.setString(7, customer.getEmailAddress());
            insert.execute();
            System.out.println("Customer with id " +  customer.getCustomerNo() + " successfully inserted");
        } catch (SQLException e) {
            System.err.println("Could not run sql statement. " + e.getMessage());
        }
    }

}