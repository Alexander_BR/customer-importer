package at.hakwt;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

public class ImportService {

    public ImportService(CustomerDao customerDao, Reader reader){
        this.customerDao = customerDao;
        this.reader = reader;
    }

    private final CustomerDao customerDao;

    private final Reader reader;

    public void importCustomers() throws IOException {
        // Reader in = new FileReader("kundendaten.csv");
        Iterable<CSVRecord> records = CSVFormat.newFormat(';').parse(this.reader);
        int i = 1;
        var customers = new ArrayList<Customer>();
        for(CSVRecord r : records) {
            if ( i == 1 ) {
                i++;
                continue;
            }
            Customer c = new Customer(Integer.parseInt(r.get(0)),
                    r.get(1),
                    r.get(2),
                    r.get(3),
                    r.get(4),
                    r.get(5),
                    r.get(6));
            customers.add(c);
        }
        this.customerDao.save(customers);
    }
}
