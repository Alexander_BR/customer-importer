package at.hakwt;

public class Customer
{
private int customerNo;

private String firstName;
private String lastName;
private String zipCode;
private String city;
private String street;
private String emailAddress;

    public Customer(int customerNo, String firstName, String lastName, String zipCode, String city, String street, String emailAddress) {
        this.customerNo = customerNo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.zipCode = zipCode;
        this.city = city;
        this.street = street;
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getCustomerNo()
    {
        return customerNo;
    }
}
