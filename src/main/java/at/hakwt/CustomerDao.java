package at.hakwt;
import java.util.List;

public interface CustomerDao {
    void save(List<Customer> customers);
}
